//
//  Game.swift
//  arrocha
//
//  Created by Mauricio on 12/11/2020.
//  Copyright © 2020 Mauricio. All rights reserved.
//

import Foundation

class Arrocha: NSObject {
    var min: Int
    var max: Int
    var num: Int
    
    override init() {
        self.min = 1
        self.max = 100
        self.num = Int(arc4random_uniform(97) + 2)
        super.init()
    }
    
    func reiniciar() {
        self.min = 0
        self.max = 100
        self.num = Int(arc4random_uniform(97) + 2)
    }
    
    func chute(numero: Int) -> [Bool] {
        if (numero == self.num || numero <= self.min || numero >= self.max) {
            return  [false, false]
        } else if (numero < self.num) {
            self.min = numero
        } else {
            self.max = numero
        }
        
        if ((self.max - self.min) == 2) {
            return [true, true]
        } else {
            return [true, false]
        }
    }
    
    override var description: String {
        return "\(self.min) - \(self.max)"
    }
}

//
//  ViewController.swift
//  arrocha
//
//  Created by Mauricio on 11/11/2020.
//  Copyright © 2020 Mauricio. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    var game: Arrocha
    
    required init?(coder aDecoder: NSCoder) {
        self.game = Arrocha()
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbIntervalo.text = game.description
    }

    @IBAction func btJogar(_ sender: Any) {
        let texto = tvChute.text
        let num = Int(texto!)
        let valido = game.chute(numero: num!)
        if(valido[0]) {
            if(valido[1]) {
                self.mostrarResultado(mensagem: "GANHOU")
                self.game.reiniciar()
            }
        } else {
            self.mostrarResultado(mensagem: "PERDEU")
            self.game.reiniciar()
        }
        lbIntervalo.text = game.description
    }
    
    func mostrarResultado(mensagem: String){
        let rvc = self.storyboard?.instantiateViewController(withIdentifier: "resultado") as! ResultadoViewController
        rvc.resultado = mensagem
        self.present(rvc, animated: true, completion: nil)
    }
    
    
    
    @IBOutlet weak var lbIntervalo: UILabel!
    @IBOutlet weak var tvChute: UITextField!
}


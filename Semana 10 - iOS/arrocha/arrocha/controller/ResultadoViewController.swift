//
//  ResultadoViewController.swift
//  arrocha
//
//  Created by Mauricio on 12/11/2020.
//  Copyright © 2020 Mauricio. All rights reserved.
//

import UIKit

class ResultadoViewController: UIViewController {
    
    var resultado: String?

    @IBOutlet weak var lbResultado: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if let resultado = resultado {
            self.lbResultado.text = resultado
        }
    }

    @IBAction func btReiniciar(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

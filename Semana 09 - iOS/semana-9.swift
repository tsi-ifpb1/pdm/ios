import Foundation

class Arrocha: NSObject {
  var min: Int
  var max: Int
  var num: Int

  override init() {
    self.min = 0
    self.max = 100
    self.num = Int.random(in: 1..<100)
    super.init()
  }

  func reiniciar() {
    self.min = 0
    self.max = 100
    self.num = Int.random(in: 1..<100)
  }

  func chute(numero: Int) -> String {
    if (numero == self.num || numero <= self.min || numero >= self.max) {
      return self.resultado(valido:false, chute: numero)
    } else if (numero < self.num) {
      self.min = numero
      if ((self.max - self.min) == 2) {
        return self.vitoria(num: self.num)
      } else {
        return self.resultado(valido: true, chute: numero)
      }
    } else {
      self.max = numero
      if ((self.max - self.min) == 2) {
        return self.vitoria(num: self.num)
      } else {
        return self.resultado(valido: true, chute: numero)
      }
    }
  }

  func resultado(valido: Bool, chute: Int) -> String {
    if (valido) {
      return "=================\nSeu chute foi \(chute)\n-----------------\n\(self.description)"
    } else {
      return "=================\nSeu chute foi \(chute)\n-----------------\n\(self.derrota(num: self.num))\n"
    }
  }

  func vitoria(num: Int) -> String {
    return "Você venceu, o número \(num) foi arrochado\n================="
  }

  func derrota(num: Int) -> String {
    return "Você perdeu, o número era \(num)\n================="
  }

  override var description: String {
    return "\(self.min) - \(self.max)\n=================\n"
  }
}

// Essa parte devera ser manipulada pelo cliente

var jogo = Arrocha()
print(jogo.chute(numero:6))
print(jogo.chute(numero:80))
print(jogo.chute(numero:40))
print(jogo.chute(numero:11))
print(jogo.chute(numero:9))